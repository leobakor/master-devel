### What is this repository for? ###

This is my first big Python project. Built on DearPyGui
0.0.1b
Discord: Leobakor#3367

### How do I get set up? ###

Dependencies: DearPyGui
Get Started:
\- Download Source
\- Install Dependencies
\- Run main.py

### Contribution guidelines ###

\- Clone
\- Pull Request. Me as reviewer!

### Who do I talk to? ###

Me is Leobakor, Talk to me on Discord: Leobakor#3367