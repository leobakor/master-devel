from dearpygui.core import *
from dearpygui.simple import *

import uuid

logged_in = False


class TodoApp:
    def __init__(self, messages):
        self.messages = messages
        self.name = 'testUser'

    def __render(self, sender, data):
        """Run every frame to update the GUI.
        Updates the table by clearing it and inserting rows with the data.
        """
        clear_table('Messages')
        for todo in self.messages:
            add_row('Messages', [todo['sender'], todo['content'], todo['time']])

    def __add_message(self, sender, data):
        new_todo_content = get_value('new-message')
        new_todo = {'id': uuid.uuid4().hex, 'sender': self.name, 'content': new_todo_content, 'time': 'Today 15:20.25'}
        self.messages.append(new_todo)
        set_value('new-message', '')

    def get_selected_message(self):
        return self.messages[get_table_selections("Messages")[0][0]]

    def __message_click(self, sender, data):
        sel_msg = self.get_selected_message()
        add_data('selected-msg', self.messages.index(sel_msg))
        set_value('Selected message:', f"Selected message: {sel_msg['content']}")

    def try_login(self, sender, data):
        global logged_in
        print(sender)
        print(data)
        if get_value('Password') == '12345' and get_value('Username') == 'Alpha':
            delete_item('Login Window')
            self.recursively_show('Main Window')
            logged_in = True
        else:
            show_item('Incorrect Password.')

    def recursively_show(self, container):
        for item in get_item_children(container):
            if get_item_children(item):
                show_item(item)
                self.recursively_show(item)
            else:
                show_item(item)

    def show(self):
        """Start the gui."""
        with window('Login Window', no_title_bar=True, autosize=True, no_resize=True):
            set_main_window_size(800, 800)
            add_input_text('Username', hint='Username: Alpha', on_enter=True, callback=self.try_login)
            add_input_text('Password', hint='Password: 12345', password=True, on_enter=True, callback=self.try_login)
            add_button('Login', callback=self.try_login)
            add_text('Incorrect Username or Password.', color=[255, 0, 0], parent='Login Window')
            hide_item('Incorrect Username or Password.')
            set_render_callback(self.__render)

        with window("Main Window", no_title_bar=True, autosize=True, no_resize=True):
            set_main_window_size(800, 800)
            set_main_window_title("Leobakor Chat App")

            add_text("Leobakor Chat App")

            add_table('Messages', ['Sender', 'Message', 'Time'], height=200, callback=self.__message_click)
            add_separator()

            add_text("Selected message:")
            add_separator()

            add_input_text("New Message", source="new-message")
            add_button("Send", callback=self.__add_message)

            # Render Callback and Start gui
            set_render_callback(self.__render)
            hide_item('Main Window', children_only=True)

        start_dearpygui(primary_window="Main Window")


if __name__ == '__main__':
    message = [
        {'id': uuid.uuid4().hex, 'sender': 'Alexej27', 'content': 'Dear', 'time': 'Today 15:20.25'},
        {'id': uuid.uuid4().hex, 'sender': 'leobakor', 'content': 'Py', 'time': 'Today 14:42.53'},
        {'id': uuid.uuid4().hex, 'sender': 'Alexej27', 'content': 'Gui', 'time': 'Today 13:26.32'},
    ]

    chat = TodoApp(message)
    chat.show()
